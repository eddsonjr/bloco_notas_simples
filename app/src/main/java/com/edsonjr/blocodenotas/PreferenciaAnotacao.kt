package com.edsonjr.blocodenotas

import android.content.Context
import android.content.SharedPreferences


class PreferenciaAnotacao(private val context: Context){

    private val ARQUIVO = "anotacao.preferencia"

    private val CHAVE = "nome"

    //usando shared preference para salvar os dados e recuperar
    private val preferences: SharedPreferences //recuperar
    private val editor: SharedPreferences.Editor //salvar


    init {
        preferences = context.getSharedPreferences(ARQUIVO,0)
        editor = preferences.edit()
    }



    //salva as anotacoes
    fun salvarAnotacao(anotacao: String?){
        editor.putString(CHAVE,anotacao)
        editor.commit()

    }


    //recuperar a anotacao
    fun recuperarAnotacao(): String? {
        return preferences.getString(CHAVE,"")
    }



}