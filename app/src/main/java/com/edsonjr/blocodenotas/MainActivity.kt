package com.edsonjr.blocodenotas

import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.edsonjr.blocodenotas.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var  binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //pegando o contexto da classe PreferenciaAnotacao
        val preferencia = PreferenciaAnotacao(applicationContext)
        val botaoSalvar = binding.fab
        val anotacao = preferencia.recuperarAnotacao()


        //evento de click no float action button
        botaoSalvar.setOnClickListener {
            val anotacaoRecuperado = binding.editContainer.editAnotacao.text.toString()

            if(anotacaoRecuperado == ""){
                Toast.makeText(this,"Digite alguma anotação!",Toast.LENGTH_SHORT).show()
            }else{
                preferencia.salvarAnotacao(anotacaoRecuperado)
                Toast.makeText(this,"Anotação Salva com sucesso",Toast.LENGTH_SHORT).show()
            }
        }


        if(anotacao != ""){
            binding.editContainer.editAnotacao.setText(anotacao)
        }




    }
}